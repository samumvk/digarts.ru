<?php

ini_set('display_errors','on');
error_reporting(E_ALL | E_STRICT);
session_start();

error_log(E_ALL & ~E_NOTICE);
error_reporting(E_ALL & ~E_NOTICE);
ini_set("display_errors","on");
include_once "conf.php";
include_once "controller/ActionsController.php";



$controller = new ActionsController($smarty, $dbconn);

$smarty->assign("moduleContent",$controller->doService());
$smarty->display("actions.html");

$dbconn->close();
