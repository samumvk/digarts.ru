<?php
/**
 * User: Maxim Dadynsky <max@dadynsky.ru>
 */
$dbhost = 'localhost';
$dbuser = 'digarts_main';
$dbpass = '8TlkcRcPII';
$dblib = "digarts_main";
$dbconn = mysqli_connect($dbhost, $dbuser, $dbpass, $dblib);
$dbconn->set_charset("cp1251");

if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

$projectRoot = dirname($_SERVER['SCRIPT_FILENAME']);

date_default_timezone_set("Asia/Novokuznetsk");

require_once $projectRoot.'/libs/smarty/Smarty.class.php';

$smarty = new Smarty();
$smarty->template_dir= $projectRoot.'/tpls/templates';
$smarty->compile_dir = $projectRoot.'/tpls/templates_c';

$smarty->cache_dir=$projectRoot.'/tpls/cache';
$smarty->compile_check = true;
$smarty->debugging=false;