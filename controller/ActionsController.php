<?php
include_once "BaseController.php";
include_once "utils/RequestUtils.php";

include_once "dao/PageDao.php";
class ActionsController extends BaseController {
    function ActionsController($smarty, $dbconn) {
        $this->tpls = $smarty;
        $this->dbconn = $dbconn;

    }
    function doService() {
        $module = Req::getStringParameter("module");
        $view = Req::getStringParameter("view");
        switch ($view)  {
            case "news"                 : return $this->newsText(); break;
            case "article"              : return $this->articleText(); break;
            case "contactsAddres"       : return $this->contactsAddres(); break;
            case "contactsCard"         : return $this->contactsCard(); break;
            case "freinds"              : return $this->freindsBlock(); break;
            case "phone"                : return $this->phoneBlock(); break;
            case "mediaPhoto"           : return $this->mediaPhoto(); break;
            case "mediaVideo"           : return $this->mediaVideo(); break;
            case "formSubmit"           : return $this->formSubmit(); break;
            case "employeePhoto"        : return $this->employeePhoto(); break;
            case "directCours"          : return $this->directCours();break;
        }
    }
    function newsText(){
        $news_id = Req::getIntegerParameter("news_id");
        $pageDao = new PageDao($this->dbconn);
        $getNews=$pageDao->getNews($news_id);
        $this->tpls->assign("getNews",$getNews);
        return $this->tpls->fetch("fbox/getNews.html");
    }
    function articleText(){
        $id_article = Req::getIntegerParameter("id_article");
        $pageDao = new PageDao($this->dbconn);
        $getArticle=$pageDao->getArticle($id_article);
        $this->tpls->assign("getArticle",$getArticle);
        return $this->tpls->fetch("fbox/getArticle.html");
    }
    function contactsAddres(){
        $branch = Req::getIntegerParameter("branch");
        $pageDao = new PageDao($this->dbconn);
        $getContacts=$pageDao->getContacts($branch);
        $this->tpls->assign("getContacts",$getContacts);
        return $this->tpls->fetch("ajaxBox/contactsAddres.html");
    }
    function contactsCard(){
        $branch = Req::getIntegerParameter("branch");
        $pageDao = new PageDao($this->dbconn);
        $getContacts=$pageDao->getContacts($branch);
        $this->tpls->assign("getContacts",$getContacts);
        return $this->tpls->fetch("ajaxBox/contactsCard.html");
    }
    function freindsBlock(){
        $pageDao = new PageDao($this->dbconn);
        $freindsList=$pageDao->freindsList();
        $this->tpls->assign("freindsList",$freindsList);
        return $this->tpls->fetch("ajaxBox/freindsBlock.html");
    }
    function phoneBlock(){
        $action = Req::getStringParameter("action");
        switch ($action){
            case "send":
            require_once "SendMailSmtpClass.php";
            $family = Req::getStringParameter("family");
            $quest = Req::getStringParameter("quest");
            $phone = Req::getStringParameter("phone");
            $time = Req::getStringParameter("time"); 
            $mailSMTP = new SendMailSmtpClass('samumvk@yandex.ru', 'dfcz123', 'ssl://smtp.yandex.ru', 'digarts.ru', 465);
            // $mailSMTP = new SendMailSmtpClass('�����', '������', '����', '��� �����������');
            // ��������� ������
            $headers= "MIME-Version: 1.0\r\n";
            $headers .= "Content-type: text/html; charset= windows-1251\r\n"; // ��������� ������
            $headers .= "From: digarts.ru <info@digarts.ru>\r\n"; // �� ���� ������
            $result =  $mailSMTP->send('samumvk@yandex.ru', '�������� ������', '���: '.$family.', �������: '.$phone.', ������: '.$quest.'. �������� �����: '.$time.'.', $headers); // ���������� ������
            // $result =  $mailSMTP->send('���� ������', '�������� ������', '����� ������', '��������� ������');
            if($result === true){
                echo "������ ������� ����������";
            }else{
                echo "������ �� ����������. ������: " . $result;
            }
            //header('Location: index.php');
            break;
        }
        return $this->tpls->fetch("ajaxBox/phoneBlock.html");
    }
    function mediaPhoto(){
        $idImg = Req::getIntegerParameter("idImg");
        $pageDao = new PageDao($this->dbconn);
        $getMedia=$pageDao->getMedia($idImg);
        $this->tpls->assign("getMedia",$getMedia);
        return $this->tpls->fetch("fbox/getImg.html");
    }
    function mediaVideo(){
        $idImg = Req::getIntegerParameter("idImg");
        $pageDao = new PageDao($this->dbconn);
        $getMedia=$pageDao->getMedia($idImg);
        if (preg_match("/v=/i", $getMedia['img'])) {
        $url1= explode("v=", $getMedia['img']);//$url1[1] ��� ������ � �����������
        $url2= explode("&", $url1[1]);//$url2[0] ��� ������ � ����������� �����������
        $url3= explode("/", $url1[0]);//�����, �����
	$getMedia['img']='<iframe width="620" height="515" src="'.$url3[0].'//'.$url3[2].'/embed/'.$url2[0].'" frameborder="0" allowfullscreen></iframe>';
	}
        $this->tpls->assign("getMedia",$getMedia);
        return $this->tpls->fetch("fbox/getVideo.html");
    }
    function formSubmit(){
        header('Content-type: text/html; charset=windows-1251');
        $pageDao = new PageDao($this->dbconn);
        require_once "SendMailSmtpClass.php";
        $select = strip_tags(Req::getStringParameter("select"));
        $family = strip_tags(Req::getStringParameter("family"));
        $name = strip_tags(Req::getStringParameter("name"));
        $age = strip_tags(Req::getStringParameter("age"));
        $telephon = strip_tags(Req::getStringParameter("telephon"));
        $email = strip_tags(Req::getStringParameter("email"));
        $direct = Req::getStringParameter("direct");
        
        $select = iconv("utf-8", "windows-1251", $select);
        $family = iconv("utf-8", "windows-1251", $family);
        $name = iconv("utf-8", "windows-1251", $name);
        $age = iconv("utf-8", "windows-1251", $age);
        if($email=='')$email='�� ������';
        
        $date = date("Ymd");
        $year = substr($date,0,4);
        $xx = substr($date,4,2);
        $day = substr($date,6,2);
        $date=$day.'.'.$xx.'.'.$year;
        
        if($direct=='studio'){
            $addres='studio@digarts.ru';
            $pageDao->saveRequest($date, $name, $family, '', $telephon, $email, 'studio', $select);
            $textMail='����: '.$date.'. ���: '.$name.', �������: '.$family.', �������: '.$telephon.', ������: '.$select.'. E-mail: '.$email.'.';
        }
        if($direct=='scool'){
            $addres='school@digarts.ru';
            $pageDao->saveRequest($date, $name, $family, $age, $telephon, $email, 'school', $select);
            $textMail='����: '.$date.'. �.�.�.���������: '.$name.', �.�.�. ����������: '.$famili.', �������: '.$telephon.', ����: '.$select.', ������� ����������: '.$age.', E-mail: '.$email.'.';
        }
        if($direct=='courses'){
            $addres='courses@digarts.ru';
            $pageDao->saveRequest($date, $name, $family, '', $telephon, $email, 'cours', $select);
            $textMail='���� '.$date.'. ��� '.$name.', ������� '.$family.', ������� '.$telephon.' ����������� '.$select.', email: '.$email.'.';
        }
        
        $mailSMTP = new SendMailSmtpClass('samumvk@yandex.ru', 'dfcz123', 'ssl://smtp.yandex.ru', 'digarts.ru', 465);
            // $mailSMTP = new SendMailSmtpClass('�����', '������', '����', '��� �����������');
            // ��������� ������
            $headers= "MIME-Version: 1.0\r\n";
            $headers .= "Content-type: text/html; charset= windows-1251\r\n"; // ��������� ������
            $headers .= "From: digarts.ru <info@digarts.ru>\r\n"; // �� ���� ������
            $result =  $mailSMTP->send($addres, '������', $textMail, $headers); // ���������� ������
            // $result =  $mailSMTP->send('���� ������', '�������� ������', '����� ������', '��������� ������');
            if($result === true){
                echo "�������! ���� ������ �������. �� ����������� � ���� ��������.";
            }else{
                echo "�������! ���� ������ �������, ������ ������ ������������� �� ����������. ������: " . $result;
            }
    }
    function employeePhoto(){
        $idImg = Req::getIntegerParameter("idImg");
        $tables = Req::getStringParameter("tables");
        $pageDao = new PageDao($this->dbconn);
        $getEmployee=$pageDao->getEmployee($tables, $idImg);
        if($tables=='teachers')$folder='schoolimg/'; else $folder='studimg/';
        $this->tpls->assign("getEmployee", $getEmployee);
        $this->tpls->assign("folder", $folder);
        return $this->tpls->fetch("fbox/getEmployee.html");
    }
    function directCours(){
        $directId = Req::getIntegerParameter("directId");
        $pageDao = new PageDao($this->dbconn);
        $getDirectCours=$pageDao->contentById($directId);
        $this->tpls->assign("getDirectCours", $getDirectCours);
        return $this->tpls->fetch("fbox/directCours.html");
    }
}
