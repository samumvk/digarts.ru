<?php
include_once "BaseController.php";
include_once "utils/RequestUtils.php";
include_once "dao/PageDao.php";

class MainController  extends BaseController {
    function MainController($smarty, $dbconn) {
        $this->tpls = $smarty;
        $this->dbconn = $dbconn;
    }

    function doService() {
        $module = Req::getStringParameter("module");
        $view = Req::getStringParameter("view");
        switch ($view)  {
        case "directstud"           : return $this->showStudioPage(); break;
        case "directschool"         : return $this->showSchoolPage(); break;
        case "directcours"          : return $this->showCoursPage();  break;
        default                     : return $this->showMainPage();
        }

        return $this->showMainPage();
    }
//mainPage
    function showMainPage(){
        $scroll=Req::getStringParameter("scroll");
        $this->tpls->assign("scroll",$scroll);
        $this->tpls->assign("headBlock",$this->headBlock('main'));
        $this->tpls->assign("block2z",$this->getBlock2z());
        $this->tpls->assign("contentBlock",$this->getContentBlock());
        $this->tpls->assign("newsBlock",$this->getNewsBlock());
        $this->tpls->assign("articlesBlock",$this->getArticlesBlock());
        $this->tpls->assign("contactsBlock",$this->getContactsBlock());
        $this->tpls->assign("futerBlock",$this->getFuterBlock('main'));
        unset($pageDao);
        return $this->tpls->fetch("main.html");
    }
    function getBlock2z(){
        $pageDao = new PageDao($this->dbconn);
        $baner=$pageDao->banersList();
        $this->tpls->assign("baner",$baner);
        return $this->tpls->fetch("blocks/main/block2z.html");
    }
    function getContentBlock(){
        $pageDao = new PageDao($this->dbconn);
        $projectLeft=$pageDao->getProjectLeft();
        $projectRight=$pageDao->getProjectRight();
        $this->tpls->assign("projectLeft",$projectLeft);
        $this->tpls->assign("projectRight",$projectRight);
        return $this->tpls->fetch("blocks/main/contentBlock.html");
    }
    function getNewsBlock(){
        $pageDao = new PageDao($this->dbconn);
        $newsList=$pageDao->newsList();
        $this->tpls->assign("newsList",$newsList);
        return $this->tpls->fetch("blocks/main/newsBlock.html");
    }
    function getArticlesBlock(){
        $pageDao = new PageDao($this->dbconn);
        $articlesList=$pageDao->articlesList();
        $this->tpls->assign("articlesList",$articlesList);
        return $this->tpls->fetch("blocks/main/articlesBlock.html");
    }
    function getContactsBlock(){
        $pageDao = new PageDao($this->dbconn);
        $contactsList=$pageDao->contactsList();
        $this->tpls->assign("contactsList",$contactsList);
        return $this->tpls->fetch("blocks/main/contactsBlock.html");
    }
    
//studioPage
    function showStudioPage(){
        $scroll=Req::getStringParameter("scroll");
        $this->tpls->assign("scroll",$scroll);
        $this->tpls->assign("headBlock",$this->headBlock('studio'));
        $this->tpls->assign("studioTitleBlock",$this->studioTitleBlock());
        $this->tpls->assign("worksBlock",$this->worksBlock());
        $this->tpls->assign("equipmentBlock",$this->equipmentBlock());
        $this->tpls->assign("employeesBlock",$this->employeesBlock());
        $this->tpls->assign("terminsBlock",$this->terminsBlock());
        $this->tpls->assign("mediaBlock",$this->mediaBlock('studio'));
        $this->tpls->assign("formBlock",$this->formBlock('studio'));
        $this->tpls->assign("futerBlock",$this->getFuterBlock('studio'));
        unset($pageDao);
        return $this->tpls->fetch("studio.html");
    }
    function studioTitleBlock(){
        $pageDao = new PageDao($this->dbconn);
        $titleContent=$pageDao->contentById(101);
        $this->tpls->assign("title",strtolower($titleContent['title']));
        $this->tpls->assign("titleContent", $titleContent);
        return $this->tpls->fetch("blocks/studio/titleBlock.html");
    }
    function worksBlock(){
        $pageDao = new PageDao($this->dbconn);
        $worksCategoryList=$pageDao->worksCategoryList();
        $countWorksCategoryList=count($worksCategoryList);
        $i=0;
        $worksList=Array();
            while($i<$countWorksCategoryList){
                $worksList[$i]=$pageDao->worksList($worksCategoryList[$i]['description']);
                $i++;
            }
        $this->tpls->assign("worksCategoryList", $worksCategoryList);
        $this->tpls->assign("worksList", $worksList);
        return $this->tpls->fetch("blocks/studio/worksBlock.html");
    }
    function equipmentBlock(){
        $pageDao = new PageDao($this->dbconn);
        $equipmentCategoryList=$pageDao->equipmentCategoryList();
        $countEquipmentCategoryList=count($equipmentCategoryList);
        $i=0;
        $equipmentList=Array();
            while($i<$countEquipmentCategoryList){
                $equipmentList[$i]=$pageDao->equipmentList($equipmentCategoryList[$i]['description']);
                $i++;
            }
        $this->tpls->assign("equipmentCategoryList", $equipmentCategoryList);
        $this->tpls->assign("equipmentList", $equipmentList);
        return $this->tpls->fetch("blocks/studio/equipmentBlock.html");
    }
    function employeesBlock(){
        $pageDao = new PageDao($this->dbconn);
        $employeesList=$pageDao->employeesList();
        $this->tpls->assign("employeesList", $employeesList);
        return $this->tpls->fetch("blocks/studio/employeesBlock.html");
    }
    function terminsBlock(){
        $pageDao = new PageDao($this->dbconn);
        $terminsList=$pageDao->terminsList();
        $this->tpls->assign("terminsList", $terminsList);
        return $this->tpls->fetch("blocks/studio/terminsBlock.html");
    }
    
    
    //schoolPage
    function showSchoolPage(){
        $scroll=Req::getStringParameter("scroll");
        $this->tpls->assign("scroll",$scroll);
        $this->tpls->assign("headBlock",$this->headBlock('scool'));
        $this->tpls->assign("schoolTitleBlock",$this->schoolTitleBlock());
        $this->tpls->assign("directSchoolBlock",$this->directSchoolBlock());
        $this->tpls->assign("teachersBlock",$this->teachersBlock());
        $this->tpls->assign("costBlock",$this->costBlock());
        $this->tpls->assign("recommendBlock",$this->recommendBlock());
        $this->tpls->assign("mediaBlock",$this->mediaBlock('scool'));
        $this->tpls->assign("formBlock",$this->formBlock('scool'));
        $this->tpls->assign("futerBlock",$this->getFuterBlock('scool'));
        return $this->tpls->fetch("school.html");
    }
    function schoolTitleBlock(){
        $pageDao = new PageDao($this->dbconn);
        $schoolTitle=$pageDao->schoolTitle();
        $this->tpls->assign("schoolTitle", $schoolTitle);
        return $this->tpls->fetch("blocks/school/titleBlock.html");
    }
    function directSchoolBlock(){
        $pageDao = new PageDao($this->dbconn);
        $getTitle=$pageDao->contentById(19);
        $directionSchoolList=$pageDao->directionSchoolList();
        $this->tpls->assign("getTitle", $getTitle);
        $this->tpls->assign("directionSchoolList", $directionSchoolList);
        return $this->tpls->fetch("blocks/school/directBlock.html");
    }
    function teachersBlock(){
        $pageDao = new PageDao($this->dbconn);
        $teachersList=$pageDao->teachersList();
        $this->tpls->assign("teachersList", $teachersList);
        return $this->tpls->fetch("blocks/school/employeesBlock.html");
    }
    function costBlock(){
        $pageDao = new PageDao($this->dbconn);
        $rowList1=$pageDao->costRowList(1);
        $rowList2=$pageDao->costRowList(2);
        $rowList3=$pageDao->costRowList(3);
        $rowList4=$pageDao->costRowList(4);
        $rowList5=$pageDao->costRowList(5);
        $this->tpls->assign("rowList1", $rowList1);
        $this->tpls->assign("rowList2", $rowList2);
        $this->tpls->assign("rowList3", $rowList3);
        $this->tpls->assign("rowList4", $rowList4);
        $this->tpls->assign("rowList5", $rowList5);
        return $this->tpls->fetch("blocks/school/costBlock.html");
    }
    function recommendBlock(){
        $pageDao = new PageDao($this->dbconn);
        $getTitle=$pageDao->contentById(237);
        $recommendSchoolList=$pageDao->recommendSchoolList();
        $this->tpls->assign("getTitle", $getTitle);
        $this->tpls->assign("recommendSchoolList", $recommendSchoolList);
        return $this->tpls->fetch("blocks/school/recommendBlock.html");
    }
    
    
    //coursPage
    function showCoursPage(){
        $scroll=Req::getStringParameter("scroll");
        $this->tpls->assign("scroll",$scroll);
        $this->tpls->assign("headBlock",$this->headBlock('courses'));
        $this->tpls->assign("coursesTitleBlock",$this->coursesTitleBlock());
        $this->tpls->assign("directCoursBlock",$this->directCoursBlock());
        $this->tpls->assign("entrantBlock",$this->entrantBlock());
        $this->tpls->assign("mediaBlock",$this->mediaBlock('courses'));
        $this->tpls->assign("formBlock",$this->formBlock('courses'));
        $this->tpls->assign("futerBlock",$this->getFuterBlock('courses'));
        return $this->tpls->fetch("courses.html");
    }
    function coursesTitleBlock(){
        return $this->tpls->fetch("blocks/courses/titleBlock.html");
    }
    function directCoursBlock(){
        $pageDao = new PageDao($this->dbconn);
        $getTitle=$pageDao->contentById(12);
        $directionscoursMos=$pageDao->contentByCategory('directionscoursMos');
        $directionscoursSpt=$pageDao->contentByCategory('directionscoursSpt');
        $this->tpls->assign("getTitle", $getTitle);
        $this->tpls->assign("directionscoursMos", $directionscoursMos);
        $this->tpls->assign("directionscoursSpt", $directionscoursSpt);
        return $this->tpls->fetch("blocks/courses/directBlock.html");
    }
    function entrantBlock(){
        $pageDao = new PageDao($this->dbconn);
        $entrantText=$pageDao->getContentByCategory('direction');
        $entrantImg=$pageDao->contentByCategory('directimg');
        $this->tpls->assign("entrantText", $entrantText);
        $this->tpls->assign("entrantImg", $entrantImg);
        return $this->tpls->fetch("blocks/courses/entrantBlock.html");
    }

    //universal
    function headBlock($direct){
        $pageDao = new PageDao($this->dbconn);
        if($direct=='studio') $byId = 13;
        if($direct=='scool') $byId = 11;
        if($direct=='courses')$byId = 12;
        $getTitle=$pageDao->contentById($byId);
        $getPhone=$pageDao->contentById(9);
        $this->tpls->assign("getTitle",$getTitle);
        $this->tpls->assign("getPhone",$getPhone);
        $this->tpls->assign("direct",$direct);
        return $this->tpls->fetch("blocks/universal/headBlock.html");
    }
    function mediaBlock($direct){
        $pageDao = new PageDao($this->dbconn);
        $mediaPhotoList=$pageDao->mediaList($direct, 'photo');
        $mediaAudioList=$pageDao->mediaList($direct, 'audio');
        $mediaVideoList=$pageDao->mediaList($direct, 'video');
        $countMediaVideoList=count($mediaVideoList);
        $i=0;
            while($i<$countMediaVideoList){
                if (preg_match("/v=/i", $mediaVideoList[$i]['img'])) {
                    $url1= explode("v=", $mediaVideoList[$i]['img']);//$url1[1] ��� ������ � �����������
                    $url2= explode("&", $url1[1]);//$url2[0] ��� ������ � ����������� �����������
                }else if (preg_match("/<iframe/i", "$mediaVideoList[$i]['img']")) {
                    $url1= explode("embed/", $mediaVideoList[$i]['img']);//$url1[1] ��� ������ � �����������
                    $url2= explode("\" frameborder", $url1[1]);//$url2[0] ��� ������ � ����������� �����������
                    $url2= explode("?rel", $url2[0]);//$url2[0] ��� ������ � ����������� �����������  
                }
                $mediaVideoList[$i]['img']='<img data-thumb="//i2.ytimg.com/vi/'.$url2[0].'/default.jpg" src="//i2.ytimg.com/vi/'.$url2[0].'/default.jpg" data-group-key="thumb-group-0">';
                $i++;
            }
        $this->tpls->assign("mediaPhotoList", $mediaPhotoList);
        $this->tpls->assign("mediaAudioList", $mediaAudioList);
        $this->tpls->assign("mediaVideoList", $mediaVideoList);
        return $this->tpls->fetch("blocks/universal/mediaBlock.html");
    }
    function formBlock($direct){
        $pageDao = new PageDao($this->dbconn);
        $formList=$pageDao->formList($direct);
        $this->tpls->assign("direct", $direct);
        $this->tpls->assign("formList", $formList);
        return $this->tpls->fetch("blocks/universal/formBlock.html");
    }
    function getFuterBlock($direct){
        $pageDao = new PageDao($this->dbconn);
        $footer1=$pageDao->getFooters(1);
        $footer2=$pageDao->getFooters(2);
        $footer3=$pageDao->getFooters(3);
        $footer4=$pageDao->getFooters(4);
        $footer5=$pageDao->getFooters(5);
        $footer6=$pageDao->getFooters(6);
        $this->tpls->assign("direct",$direct);
        $this->tpls->assign("footer1",$footer1);
        $this->tpls->assign("footer2",$footer2);
        $this->tpls->assign("footer3",$footer3);
        $this->tpls->assign("footer4",$footer4);
        $this->tpls->assign("footer5",$footer5);
        $this->tpls->assign("footer6",$footer6);
        return $this->tpls->fetch("blocks/universal/futerBlock.html");
    }
}