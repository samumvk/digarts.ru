<?php

class BaseDAO
{
    var $dbconn;

    /** MySql */
    function execSQLSelect($sql)
    {
        $result = mysqli_query($this->dbconn,$sql);
        if ( false===$result ) {
            return "error: ". mysqli_error($this->dbconn);
        }
        if ($result!=null)
            while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                $rows[] = $row;
            }

        return isset($rows) ? $rows : null;
    }

    function execSQLSelectOne($sql,$json)
    {
        $result = mysqli_query($this->dbconn,$sql);
        $row =  ($result!=null) ? mysqli_fetch_array($result) : null;
        if (isset($row) && $row!=null)
            foreach($row as $key => $value)
                $rowUpper[strtoupper($key)] = $value;


        $ret = $json ? json_encode($rowUpper) : $rowUpper;
        return isset($rowUpper) ? $ret : null;
    }

    function execSQLUpdateMysql($sql)
    {
        try{
            mysqli_query($this->dbconn,$sql);
            error_log("SQL Update : ".$sql);
        } catch (Exception $e) {
            error_log("SQL Error : ".$sql);
        }
    }

    function execSQLUpdatePrepareMysql($sql, $params)
    {
        error_log("call execSQLUpdatePrepareMysql :".$sql);
        try{

            $stmt = mysqli_prepare($this->dbconn, $sql);
            if ($stmt === FALSE) {
                error_log("SQL : ".$sql);
                error_log(mysqli_error($this->dbconn));
            }
            call_user_func_array('mysqli_stmt_bind_param', array_merge (
                array($stmt, $this->makeTypeString($params)), $this->makeValuesReferenced($params)));
            mysqli_stmt_execute($stmt);

        } catch (Exception $e) {
            error_log("SQL Error : ".$e->getMessage());
            error_log("SQL Error : ".$sql);
        }
    }


}
