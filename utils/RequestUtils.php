<?php
class Req
{
    public static function getIntegerParameter($name)
    {
        $var = isset($_REQUEST[$name]) ? $_REQUEST[$name] : null;
        if (is_numeric($var))
            return $var;
        else
            return null;
    }
    
    public static function getStringParameter($name)
    {
        if (!isset($_REQUEST[$name])) return null;
        if (strpos($_REQUEST[$name], "insert") > 0) return null;
        if (strpos($_REQUEST[$name], "select") > 0) return null;
        if (strpos($_REQUEST[$name], "union") > 0) return null;
        if (strpos($_REQUEST[$name], "drop") > 0) return null;

        return $_REQUEST[$name];
    }

    public static function getArrayParametersByStart($startName){
        foreach($_REQUEST as $key=>$val){
            if (! (strrpos($key, $startName) === false)){
                $posStart = strrpos($key, $startName);
                $posEnd = strrpos($key, "_");
                $time = substr($key,0,$posEnd);
                $time = str_replace($startName , "",$time);
                echo ($time.":* ".$val."<br>");
            }
        }
    }

    public static function convertDateForView($date){
        if ($date=='0') return "";
        $date += 19000000;
        // in: yyyymmdd, out: mm/dd/yyyy
        //$date = substr_replace(substr_replace($date, "/", 6, 0), "/", 4, 0);
        $year = substr($date,0,4);
        $mounth = substr($date,4,2);
        $day = substr($date,6,2);
        $ddate = $mounth."/".$day."/".$year;
        return $ddate;
    }
}